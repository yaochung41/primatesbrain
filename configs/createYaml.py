from numpy.lib.arraysetops import unique
import yaml
import numpy as np
import pandas as pd

df = pd.read_csv("~/Downloads/SraRunTable.txt", sep=',', header=0)
df.select = df[["Run", "Organism"]]

hs = df.select[df.select.Organism=="Homo sapiens"].reset_index()
pp = df.select[df.select.Organism=="Pan paniscus"].reset_index()
mm = df.select[df.select.Organism=="Macaca mulatta"].reset_index()
pt = df.select[df.select.Organism=="Pan troglodytes"].reset_index()

dict_hs = [
    {"sample":[]},
    {"hisat2_index":["/data/scratch/yaochung41/reference/hisat2_index/hg38"]},
    {"gtf":["hg38.ensGene.gtf"]}
]

dict_pp = [
    {"sample":[]},
    {"hisat2_index":["/data/scratch/yaochung41/reference/hisat2_index/panPan3"]},
    {"gtf":["/data/scratch/yaochung41/gtf_files/panPan3_RefSeq.gtf"]}
]

dict_mm = [
    {"sample":[]},
    {"hisat2_index":["/data/scratch/yaochung41/reference/hisat2_index/rhemac10"]},
    {"gtf":["/data/scratch/yaochung41/gtf_files/rheMac10.ensGene.gtf"]}
]

dict_pt = [
    {"sample":[]},
    {"hisat2_index":["/data/scratch/yaochung41/reference/hisat2_index/panTro6"]},
    {"gtf":["/data/scratch/yaochung41/gtf_files/panTro6.ncbiRefSeq.gtf"]}
]


for sample in hs["Run"]:
    dict_hs[0]["sample"].append(sample)

for sample in pp["Run"]:
    dict_pp[0]["sample"].append(sample)

for sample in mm["Run"]:
    dict_mm[0]["sample"].append(sample)

for sample in pt["Run"]:
    dict_pt[0]["sample"].append(sample)

# with open("test.yaml", "w") as file:
#     documents = yaml.dump(dict_file, file)

with open("hs_hisat2.yaml", "w") as file:
    documents = yaml.dump(dict_hs, file)

with open("pp_hisat2.yaml", "w") as file:
    documents = yaml.dump(dict_pp, file)

with open("mm_hisat2.yaml", "w") as file:
    documents = yaml.dump(dict_mm, file)

with open("pt_hisat2.yaml", "w") as file:
    documents = yaml.dump(dict_pt, file)