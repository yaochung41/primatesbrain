#!/bin/bash
#SBATCH -J hisat2_featureCounts
#SBATCH -D .
#SBATCH -o snake.%j.out
#SBATCH --partition=nowick
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=750M
#SBATCH --time=10-00:00:00
#SBATCH --mail-type=end
#SBATCH --mail-user=yaochung41@gmail.com

date
hostname

snakemake -s hisat2_featureCounts.snake --cores 16 --use-conda -j 32

