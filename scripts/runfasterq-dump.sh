#!/bin/bash
#SBATCH -J down
#SBATCH -D .
#SBATCH -o down.%j.out
#SBATCH --partition=big
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=750M
#SBATCH --time=10-00:00:00
#SBATCH --mail-type=end
#SBATCH --mail-user=yaochung41@gmail.com

date
hostname

snakemake -s fasterq-dump.snake --cores 16 --use-conda

